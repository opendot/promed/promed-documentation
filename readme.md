# ProMed

The project is built around a several Docker containers that coordinate in other to create an app that accept 3d files as input, let them to be edited, and fires up the algorithm to optimize them for 3D printing operations.

The containers are coordinated by the Docker Composer file i

## Volumes

 uploads:
    name: uploads
  orient-output:
    name: orient-output
  simulation-output:
    name: simulation-output
  log:
    name: log
  shared:
    name: shared  
  app:
    name: app

## definitions

### GUI

- Definition:
	- Node.js
- Activities:
	- Deploy *HTTP server*
		- User interface
		- Upload to-process models
		- Download processed models
		- Browsing previous requests
	- Send models to *shared volume* and info to **File Manager** to retrieve them
	- Receive *orientation data*, *simulation data*, *support geometry data* from **3D model orienting**, and send to **DB manager**

### 3D model orient & process simulator

- Definition:
	- Ubuntu
- Activities:
	- Model upload
	- Model processing
	- Model simulation
	- Send *orientation data*, *simulation data*, *support geometry data* to **GUI**

### File manager

- Definition:
	- Ubuntu
- Activities:
	- Receive *call* from **GUI** 
	- Retrieve *data* from *shared drive*
	- Sends and retrieve *data* from and to *File DB*
